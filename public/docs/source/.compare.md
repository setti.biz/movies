---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://movies-api.test/docs/collection.json)

<!-- END_INFO -->

#Movies

This API routes used to create new movies, add titles
to the users personal favorites lists and to list
all movies except favorites.
<!-- START_5d08976095aebb3ebe39e57a44977987 -->
## Create new movie

Add a movie to the app's movies global database
and attach current user as a movie owner.
Returns created movie.

> Example request:

```bash
curl -X POST "http://movies-api.test/api/movie/add" \
-H "Accept: application/json" \
    -d "title"="odio" \
    -d "description"="odio" \
    -d "year"="4978" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://movies-api.test/api/movie/add",
    "method": "POST",
    "data": {
        "title": "odio",
        "description": "odio",
        "year": 4978
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "type": "movies",
    "id": "1",
    "attributes": {
        "title": "Kunde-Prosacco",
        "description": "Laudantium praesentium ratione amet...",
        "year": 1976,
        "owner_id": 1
    }
}
```

### HTTP Request
`POST api/movie/add`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    title | string |  required  | Maximum: `190` Minimum: `3`
    description | string |  required  | Minimum: `3`
    year | numeric |  required  | Must have a length between `4` and `4`

<!-- END_5d08976095aebb3ebe39e57a44977987 -->

<!-- START_88281f6983b5a7fa135e074ed43495ca -->
## Add to favorites

Add movie to the current user's favorites
list by the movie ID. Returns the added movie.

> Example request:

```bash
curl -X POST "http://movies-api.test/api/movie/add-to-favorites" \
-H "Accept: application/json" \
    -d "id"="14109" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://movies-api.test/api/movie/add-to-favorites",
    "method": "POST",
    "data": {
        "id": 14109
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "type": "movies",
    "id": "1",
    "attributes": {
        "title": "Kunde-Prosacco",
        "description": "Laudantium praesentium ratione amet ...",
        "year": 1976,
        "owner_id": 1
    }
}
```

### HTTP Request
`POST api/movie/add-to-favorites`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  required  | Valid movie id

<!-- END_88281f6983b5a7fa135e074ed43495ca -->

<!-- START_c92b453ec84c5cd240e17ba9ff345bff -->
## Get all movies

Retrieve all movies, but exclude user's favorite list

> Example request:

```bash
curl -X GET "http://movies-api.test/api/movies/none-favorite" \
-H "Accept: application/json" \
    -d "page"="86177665" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://movies-api.test/api/movies/none-favorite",
    "method": "GET",
    "data": {
        "page": 86177665
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "data": [
        {
            "id": 5,
            "title": "Stamm-Johns",
            "description": "Nihil eos et laudantium...",
            "year": 1983,
            "created_at": "2018-10-01 19:13:02"
        },
        {
            "id": 6,
            "title": "Brakus Inc",
            "description": "Quo quam non ratione...",
            "year": 1990,
            "created_at": "2018-10-01 19:13:02"
        }
    ],
    "links": {
        "first": "http:\/\/movies-api.test\/api\/movies\/none-favorite?page=1",
        "last": "http:\/\/movies-api.test\/api\/movies\/none-favorite?page=1",
        "prev": null,
        "next": null,
        "self": "http:\/\/movies-api.test\/api\/movies\/none-favorite"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/movies-api.test\/api\/movies\/none-favorite",
        "per_page": 15,
        "to": 2,
        "total": 2
    }
}
```

### HTTP Request
`GET api/movies/none-favorite`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    page | integer |  optional  | 

<!-- END_c92b453ec84c5cd240e17ba9ff345bff -->

