---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->

# Authorization

 In order to interact with this API, all requests
 should provide an API token. You can get one
 via OAuth2 endpoint
 `http://oauth.moreorless21.com/oauth/token`.
 It is uses `password` grant type and for testing purposes to get the token, you can use this credentials of
 already registered user:
 * `client_id: 2`
 * `client_secret: GrxByPP9EEmbeNGPyh5sqF9qzCPm804CLFJ0Jdr8`
 * `username: marvin@gmail.com`
 * `password: secret`
 
## Request to the OAuth2 endpoint
With provided above credentials the query to get the token should looks like this:

```
 POST http://oauth.moreorless21.com/oauth/token?grant_type=password&client_id=2&client_secret=GrxByPP9EEmbeNGPyh5sqF9qzCPm804CLFJ0Jdr8&username=marvin@gmail.com&password=secret&scope=*
Accept: application/json
```

## Response example with granted access_token
Field `access_token` is the value, we are interested in

```
{"token_type":"Bearer","expires_in":31536000,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFlZWZkNDBlY2JhNDUzZjJmM2E1YmE5ZTdhZWRjZWMyYjQwZTI1MWY2ZDMyMTdhYThjZTUwNmQwMzdjNDNkYmM1OGMyOWUxYzUxOTI5NjdjIn0.eyJhdWQiOiIyIiwianRpIjoiYWVlZmQ0MGVjYmE0NTNmMmYzYTViYTllN2FlZGNlYzJiNDBlMjUxZjZkMzIxN2FhOGNlNTA2ZDAzN2M0M2RiYzU4YzI5ZTFjNTE5Mjk2N2MiLCJpYXQiOjE1Mzg0NTk4NjgsIm5iZiI6MTUzODQ1OTg2OCwiZXhwIjoxNTY5OTk1ODY4LCJzdWIiOiI3NzciLCJzY29wZXMiOlsiKiJdfQ.xXKlud8qvYVmTdUOcBWtP3ubxmvktIOhl_EnEgR0i2VJUwmp7Z-svft2WYC3MhaqxCdjGz2kdc0zPBQni6lbYnBQCOcmO9yn8kb82rIydnHlFHlxHwzAa-aCSfnlVClG5dbf9QI6FW2aY0D-yfYVDXaFYFB4Wk9iBLzjpHNHdqFqNxlLbdlIh-907k7H3sCveB7hGvfSy2s3TXtiTcIIUH_gjW24z80xGY38SUXxe2BPMTGJ3M7B5XHvd4QXrWRqZkC8dT33-QUy_dmuxhhgrjscUNFjDflowUMQnUK0rPstBDuC4xSr0fQqzSvtI086hH3phOpct4O32aFquI4ip2I7pQE2Wnu8DhMTfZ6UFhZw6arIUNuN8FdzjQkHCe1EZ0yqXI-ILOBGJPJccsjoAwTiLSS1qvn1TzqA0WoYGKKUKqiPeH2Cy5dN1_Uc8RuJeV5WbeTeK1rjiTUmwTm0NcDrmORSGIXaZIQPnbfS_m3sGt2HZvss3zH6Y2Nvb9Im9ATnGt-Hx4skXnMBeasHkhMINRbGCSbzmZganPNJ6hXqHeVpu7Y3MeTmRkqo2pv5RzEt6xqUYqRPX-7ErNNTTeeERs-pMdOsYc_aGmKeqtL5dM9dPWsWp99VU8UnCePbiNko_JD2zjWIadUgNowKfuzKvnpqG6i75qn8TukslSc","refresh_token":"def50200415b30a947c386790b3d83a00a1b91ffb6cbf9d8afae66f4ff6baecefa0a24500b64eb63a74432b75ec916e6740eb608d7bf9079aee2df70ee7a65ed22bdbc6dbf6fef920f4cd67f99486dd14c05e1f27327f1f2599a1bad1d2da9b3a098e7a04637df0f025b59d6134ea3f4b095d9b41693d5c5a8118c0414b03cb475f729ac9c9e8ef65bace80d57ecff5187430b6018ccfcb3a25832a528a339d48c31ec42e74b4acb1c1a3e326f84425ade6e16cb3358c81ec455a869ecac17d10219d5c709f54bef9fc99323a5d1404cd4c95ca14bc5a401a142b738e16764ad4a3101c1fe5184d1b3a24d1878ee39d642e8c03bc772a20a735580a321b3f16aa12376fef65a3b8744a60d2ac9708fae58112b912e7868048a2e7fe2fdfeb58985c61fde9d740ae6600ea36715df967597a6cc1390ad4fd02151688574b0c4a098c306c8d1320a8859f0e2e4ccb718022e9ac662610d0913fdd93256a8536be0f62c8574f5af"}
```
## Request example with access_token header
So, with `access_token` from example above your next queries to the Movies API will be signed with `Authorization` HTTP header. Prefix `Bearer ` is necessary. Something like this:

```GET http://movies.moreorless21.com/api/movies/none-favorite
Cache-Control: no-cache
Accept: application/json
Authorization: Bearer eyJ0eXAiOiJKV1Q...
```

<!-- END_INFO -->

#Movies
<!-- START_5d08976095aebb3ebe39e57a44977987 -->
## Create new movie

Add a movie to the app's movies global database
and attach current user as a movie owner.
Returns created movie.

> Example request:

```bash
curl -X POST "http://movies-api.test/api/movie/add" \
-H "Accept: application/json" \
    -d "title"="odio" \
    -d "description"="odio" \
    -d "year"="4978" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://movies-api.test/api/movie/add",
    "method": "POST",
    "data": {
        "title": "odio",
        "description": "odio",
        "year": 4978
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "type": "movies",
    "id": "1",
    "attributes": {
        "title": "Kunde-Prosacco",
        "description": "Laudantium praesentium ratione amet...",
        "year": 1976,
        "owner_id": 1
    }
}
```

### HTTP Request
`POST api/movie/add`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    title | string |  required  | Maximum: `190` Minimum: `3`
    description | string |  required  | Minimum: `3`
    year | numeric |  required  | Must have a length between `4` and `4`

<!-- END_5d08976095aebb3ebe39e57a44977987 -->

<!-- START_88281f6983b5a7fa135e074ed43495ca -->
## Add to favorites

Add movie to the current user's favorites
list by the movie ID. Returns the added movie.

> Example request:

```bash
curl -X POST "http://movies-api.test/api/movie/add-to-favorites" \
-H "Accept: application/json" \
    -d "id"="14109" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://movies-api.test/api/movie/add-to-favorites",
    "method": "POST",
    "data": {
        "id": 14109
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "type": "movies",
    "id": "1",
    "attributes": {
        "title": "Kunde-Prosacco",
        "description": "Laudantium praesentium ratione amet ...",
        "year": 1976,
        "owner_id": 1
    }
}
```

### HTTP Request
`POST api/movie/add-to-favorites`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  required  | Valid movie id

<!-- END_88281f6983b5a7fa135e074ed43495ca -->

<!-- START_c92b453ec84c5cd240e17ba9ff345bff -->
## Get all movies

Retrieve all movies, but exclude user's favorite list

> Example request:

```bash
curl -X GET "http://movies-api.test/api/movies/none-favorite" \
-H "Accept: application/json" \
    -d "page"="86177665" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://movies-api.test/api/movies/none-favorite",
    "method": "GET",
    "data": {
        "page": 86177665
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "data": [
        {
            "id": 5,
            "title": "Stamm-Johns",
            "description": "Nihil eos et laudantium...",
            "year": 1983,
            "created_at": "2018-10-01 19:13:02"
        },
        {
            "id": 6,
            "title": "Brakus Inc",
            "description": "Quo quam non ratione...",
            "year": 1990,
            "created_at": "2018-10-01 19:13:02"
        }
    ],
    "links": {
        "first": "http:\/\/movies-api.test\/api\/movies\/none-favorite?page=1",
        "last": "http:\/\/movies-api.test\/api\/movies\/none-favorite?page=1",
        "prev": null,
        "next": null,
        "self": "http:\/\/movies-api.test\/api\/movies\/none-favorite"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/movies-api.test\/api\/movies\/none-favorite",
        "per_page": 15,
        "to": 2,
        "total": 2
    }
}
```

### HTTP Request
`GET api/movies/none-favorite`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    page | integer |  optional  | 

<!-- END_c92b453ec84c5cd240e17ba9ff345bff -->

