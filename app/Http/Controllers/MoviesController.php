<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddMovieRequest;
use App\Http\Requests\AddMovieToFavoritesRequest;
use App\Http\Requests\GetNoneFavoriteMoviesRequest;
use App\Http\Resources\MovieResource;
use App\Http\Resources\MoviesResource;
use App\Movie;


/**
 * @resource Movies
 *
 * This API routes used to create new movies, add titles
 * to the users personal favorites lists and to list
 * all movies except favorites.
 *
 */
class MoviesController extends Controller
{
    /**
     * Create new movie
     *
     * Add a movie to the app's movies global database
     * and attach current user as a movie owner.
     * Returns created movie.
     *
     * @param AddMovieRequest $request
     * @return MovieResource
     *
     * @response {"type":"movies","id":"1","attributes":{"title":"Kunde-Prosacco","description":"Laudantium praesentium ratione amet...","year":1976,"owner_id":1}}
     *
     */
    public function add(AddMovieRequest $request)
    {
        $data = array_merge($request->only([
            'title',
            'description',
            'year',
        ]), ['owner_id' => \Auth::user()->id]);

        $movie = Movie::create($data);

        return new MovieResource($movie);
    }

    /**
     * Add to favorites
     *
     * Add movie to the current user's favorites
     * list by the movie ID. Returns the added movie.
     *
     * @param AddMovieToFavoritesRequest $request
     * @return MovieResource
     *
     * @response {"type":"movies","id":"1","attributes":{"title":"Kunde-Prosacco","description":"Laudantium praesentium ratione amet ...","year":1976,"owner_id":1}}
     *
     */
    public function addToFavorites(AddMovieToFavoritesRequest $request)
    {
        $user = \Auth::user();

        $movie = Movie::find($request->input('id'));

        $user->favoriteMovies()->save($movie);

        return new MovieResource($movie);
    }

    /**
     * Get all movies
     *
     * Retrieve all movies, but exclude user's favorite list
     *
     * @param GetNoneFavoriteMoviesRequest $request
     * @return MoviesResource
     *
     * @response {
    "data": [
        {
            "id": 5,
            "title": "Stamm-Johns",
            "description": "Nihil eos et laudantium...",
            "year": 1983,
            "created_at": "2018-10-01 19:13:02"
        },
        {
            "id": 6,
            "title": "Brakus Inc",
            "description": "Quo quam non ratione...",
            "year": 1990,
            "created_at": "2018-10-01 19:13:02"
        }
        ],
        "links": {
            "first": "http:\/\/movies-api.test\/api\/movies\/none-favorite?page=1",
            "last": "http:\/\/movies-api.test\/api\/movies\/none-favorite?page=1",
            "prev": null,
            "next": null,
            "self": "http:\/\/movies-api.test\/api\/movies\/none-favorite"
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "path": "http:\/\/movies-api.test\/api\/movies\/none-favorite",
            "per_page": 15,
            "to": 2,
            "total": 2
        }
    }
     */
    public function getNoneFavoriteMovies(GetNoneFavoriteMoviesRequest $request)
    {
        $movies = Movie\MovieRepository::create()
            ->moviesWithoutFavoritesQuery()
            ->paginate();

        return new MoviesResource($movies);
    }
}
