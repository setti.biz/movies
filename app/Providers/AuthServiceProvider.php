<?php

namespace App\Providers;

use App\User;
use Firebase\JWT\JWT;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::viaRequest('custom-token', function ($request) {
            $token = request()->header('authorization');
            $publicKey = file_get_contents(storage_path('oauth-public.key'));

            if (! $hasAuthHeader = $token ? true : false) {
                return null;
            }

            preg_match('/Bearer\s((.*)\.(.*)\.(.*))/', $token, $jwt);

            try {
                $res = JWT::decode($jwt[1], $publicKey, array('RS256'));

                $user = User::getOrCreateUserByExternalId($res->sub);

                return $user;
            } catch (\Exception $e) {
                return null;
            }
        });
    }


}
