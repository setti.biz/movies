<?php


namespace App\Movie;


use App\Movie;
use Illuminate\Database\Query\JoinClause;

/**
 * @property \App\User|null currentUser
 */
class MovieRepository
{
    public static function create()
    {
        $repository = new self();
        $repository->currentUser = \Auth::user();

        return $repository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function allMoviesQuery()
    {
        return Movie::query();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function moviesWithoutFavoritesQuery()
    {
        if (! $this->currentUser) {
            throw new \Error('You must be logged in to get movies without favorites');
        }

        return Movie::query()->select(
            [
                'id',
                'title',
                'description',
                'year',
                'created_at'
            ])->leftJoin('favorite_movies', function ($join) {
            /** @var JoinClause $join */
            $join->on(
                'movies.id',
                '=',
                'favorite_movies.movie_id'
            )->where(
                'favorite_movies.user_id',
                '=',
                $this->currentUser->id
            );
        })->whereNull('favorite_movies.movie_id');
    }
}
