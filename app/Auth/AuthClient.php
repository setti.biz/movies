<?php


namespace App\Auth;


use GuzzleHttp\Client;

class AuthClient
{
    /**
     * @var Client
     */
    protected $http;

    /**
     * AuthClient constructor.
     * @param Client $http
     */
    public function __construct(Client $http)
    {
        $this->http = $http;
    }

    public function retrieveToken($username, $password, $scope = '*')
    {
        $connectionConfig = [
            'grant_type' => 'password',
            'client_id' => config('auth.oauth_client_id'),
            'client_secret' => config('auth.oauth_client_secret'),
            'username' => $username,
            'password' => $password,
            'scope' => $scope,
        ];

        $response = $this->http->post(
            config('auth.oauth_url'),
            ['form_params' => $connectionConfig]
        );

        $contents = $response->getBody()->getContents();
        $token = json_decode($contents);

        return $token->access_token ?? null;
    }
}
