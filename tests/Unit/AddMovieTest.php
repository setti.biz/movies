<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AddMovieTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddMovie()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $movieTitle = 'The Godfather';
        $movieDescription = $movieTitle . ' description...';

        $response = $this->postJson('/api/movie/add', [
            'title' => $movieTitle,
            'description' => $movieDescription,
            'year' => 1972
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('movies', [
            'title' => $movieTitle,
            'description' => $movieDescription,
            'year' => 1972
        ]);
    }

    public function testAddMovieWithInvalidData()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->postJson('/api/movie/add', [
            'title' => '',
            'description' => 'd',
            'year' => 1
        ]);

        $response->assertJsonValidationErrors([
            'title',
            'description',
            'year'
        ]);
    }

}
