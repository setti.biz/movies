<?php

namespace Tests\Unit;

use App\Movie;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FavoriteMoviesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddMovieToFavoritesDirectlyWithModel()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $movie = factory(Movie::class)->create();

        $user->favoriteMovies()->save($movie);

        $favorites = $user->favoriteMovies;

        $this->assertEquals(1, $favorites->count());
    }

    public function testAddMovieToFavoritesViaApi()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        /** @var Movie $movie */
        $movie = factory(Movie::class)->create();

        $response = $this->postJson('/api/movie/add-to-favorites', [
            'id' => 'wrongId',
        ]);

        $response->assertJsonValidationErrors([
            'id',
        ]);

        $response = $this->postJson('/api/movie/add-to-favorites', [
            'id' => $movie->id,
        ]);

        $response->assertOk();
    }

    public function testMoviesWithoutFavorite()
    {
        /** @var User $user1 */
        $user1 = factory(User::class)->create();
        $this->actingAs($user1, 'api');

        $user2 = factory(User::class)->create();

        $movie1 = factory(Movie::class)->create();
        $movie2 = factory(Movie::class)->create();
        $movie3 = factory(Movie::class)->create();

        $user1->favoriteMovies()->save($movie1);
        $user2->favoriteMovies()->save($movie1);

        $favorites = $user1->favoriteMovies;

        $this->assertEquals(1, $favorites->count());

        $allMoviesWithFavorites = Movie\MovieRepository::create()
            ->allMoviesQuery()->get();

        $this->assertEquals(3, $allMoviesWithFavorites->count());

        $allMoviesWithoutFavorites = Movie\MovieRepository::create()
            ->moviesWithoutFavoritesQuery()->get();

        $this->assertEquals(2, $allMoviesWithoutFavorites->count());

        $response = $this->getJson('/api/movies/none-favorite', [
            'page' => 1
        ]);

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(2, $data['meta']['total']);

        $response->assertOk();
    }

}
