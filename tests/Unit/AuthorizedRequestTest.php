<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;

class AuthorizedRequestTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetMe()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->get('/api/user');
        $response->assertJsonFragment(['id' => $user->id]);
        $response->assertStatus(201);
    }

}
