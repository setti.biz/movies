<?php

namespace Tests\Unit;

use App\Auth\AuthClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Tests\TestCase;

class TokenTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetTokenViaClient()
    {
        /** @var AuthClient $client */
        $client = app()->make(AuthClient::class);
        $token = $client->retrieveToken('marvin@gmail.com', 'secret');

        $this->assertInternalType('string', $token);
        $this->assertNotEmpty($token);
    }

    public function testGetTokenViaClientWithWrongCredentials()
    {
        /** @var AuthClient $client */
        $client = app()->make(AuthClient::class);

        $this->expectException(BadResponseException::class);

        $client->retrieveToken('marvin@gmail.com', '');
    }

    public function testGetTokenManually()
    {
        $http = new Client();

        $response = $http->post('http://oauth.moreorless21.com/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => 2,
                'client_secret' => 'GrxByPP9EEmbeNGPyh5sqF9qzCPm804CLFJ0Jdr8',
                'username' => 'marvin@gmail.com',
                'password' => 'secret',
                'scope' => '*',
            ],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
