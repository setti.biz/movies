<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Marvin Martian',
                'email' => 'marvin@gmail.com',
                'password' => bcrypt('secret'),
                'external_id' => 777,
            ],
            [
                'name' => 'Karen D. McBride',
                'email' => 'karen@gmail.com',
                'password' => bcrypt('secret'),
                'external_id' => 778,
            ],
        ]);
    }
}
