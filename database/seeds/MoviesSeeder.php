<?php

use Illuminate\Database\Seeder;

class MoviesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Movie::class)->create();
        factory(\App\Movie::class)->create();
        factory(\App\Movie::class)->create();

        $user = \App\User::first();
        $user->favoriteMovies()->save(\App\Movie::first());
    }
}
