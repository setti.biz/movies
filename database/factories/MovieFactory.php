<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->company,
        'description' => $faker->text,
        'year' => $faker->year,
        'owner_id' => \App\User::first()->id,
    ];
});
